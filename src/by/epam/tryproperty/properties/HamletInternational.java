package by.epam.tryproperty.properties;

import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * @author Igor Pavlyushchik
 *         Created on February 25, 2015.
 */
public class HamletInternational {
    public static void main( String[] args ) {
	for (int i = 0; i < 3; i++) {
	    System.out.println("1 - английский \n2 - белорусский \nлюбой - русский ");
	    char ch = 0;
	    char ignore = 0;
	    try {
		ch = (char) System.in.read();
		do {
		    ignore = (char) System.in.read();
		} while(ignore != '\n');
	    } catch (IOException e) {
		e.printStackTrace();
	    }
	    String country = "";
	    String language = "";
	    switch (ch) {
		case '1' :
		    country = "US";
		    language = "EN";
		    break;
		case '2' :
		    country = "BY";
		    language = "BE";
		    break;
	    }
	    Locale current = new Locale(language, country);
	    System.out.println("\n" + System.getProperty("user.dir"));
	    ResourceBundle resourceBundle = ResourceBundle.getBundle("property.text", current);
	    String s1 = resourceBundle.getString("str1");
	    String s2 = resourceBundle.getString("str2");
	    System.out.println(s1);
	    System.out.println(s2);
	}
    }
}
